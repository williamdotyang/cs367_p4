///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  Scheduler.java
// File:             IntervalBST.java
// Semester:         CS367 Fall 2015
//
// Author:           Weilan Yang, wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ////////////////////
//
// Pair Partner:     Huilin Hu
// Email:            hhu28@wisc.edu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////////////// 80 columns wide //////////////////////////////////
import java.util.Iterator;


/**
 * An IntervalBST is a modified Binary Search Tree that has an Event object as 
 * its key and uses the start time of the Event to sort its nodes.
 * 
 * @author Weilan Yang
 */
public class IntervalBST<K extends Interval> implements SortedListADT<K> {
	// fields
	private IntervalBSTnode<K> root;
    private int numItems;
	
    // constructor
    public IntervalBST() {
    	root = null;
    	numItems = 0;
	}

    // methods
    /**
     * See SortedListADT
     */
	public void insert(K key){
		root = insert(key, root);
	}
	
	/**
	 * Companion method for insert()
	 * @param key The item to be inserted
	 * @param root The current node
	 */
	private IntervalBSTnode<K> insert(K key, IntervalBSTnode<K> root) {
		if (root == null) {
			numItems++;
			return new IntervalBSTnode<K>(key);
		}
		
		K rootKey = root.getData();
		if (key.compareTo(rootKey) < 0 && !key.overlap(rootKey)) { 
			root.setLeft(insert(key, root.getLeft()));
			return root;
		}
		if (key.compareTo(rootKey) > 0 && !key.overlap(rootKey)) { 
			root.setRight(insert(key, root.getRight()));
			return root;
		}
		
		throw new IntervalConflictException();
	}

	/**
     * See SortedListADT
     */
	public boolean delete(K key) {
		int temp = numItems;
		root = delete(key, root);
		if (temp != numItems) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Companion method for delete()
	 * @param key The item to be deleted
	 * @param root The current node
	 * @return The root tree node after deleting
	 */
	private IntervalBSTnode<K> delete(K key, IntervalBSTnode<K> root) {
		// base case: no match
		if (root == null) return null;
		// base case: matched
		if (key.compareTo(root.getData()) == 0) {
			numItems--;
			// case: no child
			if (root.getLeft() == null && root.getRight() == null) return null;
			// case: two children 
			if (root.getLeft() != null && root.getRight() != null) {
				IntervalBSTnode<K> subTree = root.getLeft();
				// in-order predecessor
				while (subTree.getRight() != null) subTree = subTree.getRight();
				K replace = subTree.getData();
				root.setKey(replace);
				root.setLeft(delete(replace, root.getLeft()));
				return root;
			}
			// case: one child
			else {
				if (root.getLeft() != null) return root.getLeft();
				else return root.getRight();
			}
		}
		// recursive call
		if (key.compareTo(root.getData()) < 0) {
			root.setLeft(delete(key, root.getLeft()));
			return root;
		} else {
			root.setRight(delete(key, root.getRight()));
			return root;
		}
	}
	
	/**
     * See SortedListADT
     */
	public K lookup(K key) {
		IntervalBSTnode<K> lookedup = lookup(key, root);
		return lookedup == null ? null : lookedup.getData();
	}
	
	/**
	 * Companion method for lookup()
	 * @param key The item to be looked up
	 * @param root The current node
	 * @return The node containing the key from the Sorted List, if it is 
	 * there; null if the key is not in the Sorted List.
	 */
	private IntervalBSTnode<K> lookup(K key, IntervalBSTnode<K> root) {
		if (root == null) return null;
		if (key.equals(root.getData())) return root;
		if (key.compareTo(root.getData()) < 0) 
			return lookup(key, root.getLeft());
		else
			return lookup(key, root.getRight());
	}

	/**
     * See SortedListADT
     */
	public int size() {
		return numItems;
	}

	/**
     * See SortedListADT
     */
	public boolean isEmpty() {
		return numItems == 0;
	}

	/**
     * See SortedListADT
     */
	public Iterator<K> iterator() {
		return new IntervalBSTIterator<K>(root);
	}

}