///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  Scheduler.java
// File:             IntervalBSTnode.java
// Semester:         CS 367 Fall 2015
//
// Author:           Huilin Hu
// CS Login:         huilin	
// Lecturer's Name:  Jim Skrentny
// Lab Section:      Section 2
/////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ////////////////////
//
// Pair Partner:     Weilan Yang
// Email:            wyang65@wisc.edu	
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      Section 2
///////////////////////////////////////////////////////////////////////////////

/**
 * A node of the IntervalBST contains an object of type Interval interface as its key. 
 *
 * @author Huilin Hu
 *
 * @param <K>
 */
class IntervalBSTnode<K extends Interval> {
	// fields
	private K keyValue;
	private IntervalBSTnode<K> leftChild;
	private IntervalBSTnode<K> rightChild;
	private long maxEnd;
	
	/**
	 * Constructs a node of the IntervalBST that contains an object of type Interval 
	 * interface as its key. 
	 * @param keyValue Key value
	 */
    public IntervalBSTnode(K keyValue) {
    	this.keyValue = keyValue;
    }
    
    /**
     * Constructs a node of the IntervalBST that contains an object of type Interval 
	 * interface as its key, and has left and right children
     * @param keyValue Key value
     * @param leftChild left child
     * @param rightChild right child
     * @param maxEnd Maximum end value of the intervals 
     */
    public IntervalBSTnode(K keyValue, IntervalBSTnode<K> leftChild, IntervalBSTnode<K> rightChild, long maxEnd) {
		this.keyValue = keyValue;
		this.leftChild = leftChild;
		this.rightChild = rightChild;
		this.maxEnd = maxEnd;
    }

    /**
     * Returns the key value for this BST node.
     * @return the key value for this BST node.
     */
    public K getKey() { 
		return keyValue;
    }
    
    /**
     * Returns the left child for this BST node.
     * @return the left child for this BST node.
     */
    public IntervalBSTnode<K> getLeft() { 
		return leftChild;
    }
  
    /**
     * Returns the right child for this BST node.
     * @return the right child for this BST node.
     */
    public IntervalBSTnode<K> getRight() { 
		return rightChild;
    }
 
    /**
     * Returns the maximum end value of the intervals in this node's subtree.
     * @return the maximum end value of the intervals in this node's subtree.
     */
    public long getMaxEnd(){
		return maxEnd;
    }
 
    /**
     * Changes the key value for this node to the one given.
     * @param newK New key value
     */
    public void setKey(K newK) { 
		this.keyValue = newK;
    }
    
    /**
     * Changes the left child for this node to the one given.
     * @param newL New left child
     */
    public void setLeft(IntervalBSTnode<K> newL) { 
		this.leftChild = newL;
    }
    
    /**
     * Changes the right child for this node to the one given.
     * @param newR New right child
     */
    public void setRight(IntervalBSTnode<K> newR) { 
		this.rightChild = newR;
    }
    
    /**
     * Changes the maxEnd to the updated maximum end in the subtree.
     * @param newEnd New maxEnd
     */
    public void setMaxEnd(long newEnd) { 
		this.maxEnd = newEnd;
    }
    
    /**
     * Returns the start timestamp of this interval.
     * @return the start timestamp of this interval.
     */
    public long getStart(){ 
		return keyValue.getStart();
	}

    /**
     * Returns the end timestamp of this interval.
     * @return The end timestamp of this interval.
     */
    public long getEnd(){
		return keyValue.getEnd();
	}

    /**
     * Returns the key value of this BST node.
     * @return The key value of this BST node.
     */
    public K getData(){
		return keyValue;
	}
    
}