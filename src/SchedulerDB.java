///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  Scheduler.java
// File:             SchedulerDB.java
// Semester:         CS 367 Fall 2015
//
// Author:           Huilin Hu
// CS Login:         huilin	
// Lecturer's Name:  Jim Skrentny
// Lab Section:      Section 2
/////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ////////////////////
//
// Pair Partner:     Weilan Yang
// Email:            wyang65@wisc.edu	
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      Section 2
//
///////////////////////////////////////////////////////////////////////////////

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;
import java.lang.*;

/**
 * The SchedulerDB object maintains a list of Resource objects.
 * 
 * @author Huilin Hu
 */
public class SchedulerDB {
	List<Resource> resourceList = new ArrayList<Resource> () ;
	
	/**
	 * Constructs a SchedulerDB object.
	 * @param none
	 */
	SchedulerDB(){
		resourceList = new ArrayList<Resource>();
	}
	
	/**
	 * Returns true if the resource has been added, false otherwise
	 * @param resource Name of the resource
	 * @return true if the resource has been added
	 */
	public boolean addResource(String resource){
		Iterator<Resource> resItr = resourceList.iterator();
		while (resItr.hasNext())	{
			Resource resObj = resItr.next();
			if (resObj.getName().equals(resource))	
				return false;
				
		}
		resourceList.add(new Resource(resource));
		return true;
		
	}
	
	/**
	 * Returns true if the resource has been removed, false otherwise
	 * @param r Name of the resource
	 * @return true if the resource has been removed
	 */
	public boolean removeResource(String r){
		Iterator<Resource> resItr = resourceList.iterator();
		while (resItr.hasNext())	{
			Resource resObj = resItr.next();
			if (resObj.getName().equals(r))	{
				return 
					resourceList.remove(resObj);
				
			}
		}
		
		return false;
			
	}
	
	/**
	 * Returns true if a new event can be added to a resource.
	 * @param r Name of the resource
	 * @param start Start time of the event
	 * @param end End time of the event
	 * @param name Name of the event
	 * @param organization By which the event is organized
	 * @param description Description to the event
	 * @return true if a new event can be added to a resource.
	 * @throw IntervalConflictException if the new event overlaps an existing event
	 */
	public boolean addEvent(String r, long start, long end, String name, 
			String organization, String description){
		Event newEvent = new Event(start, end, name, r, organization, description);	
		
		Resource targetResource = null;
		Iterator<Resource> resItr = resourceList.iterator();
		while (resItr.hasNext())	{
			Resource resObj = resItr.next();
			if (resObj.getName().equals(r))		
				targetResource = resObj;	
		}
		
		if (targetResource == null)	 return false;
			
		Iterator<Event> itr = targetResource.iterator();
		
		while (itr.hasNext())	{
			Event e = itr.next();
			if (e.overlap(newEvent))	
				throw new IntervalConflictException();
		}
		
		return targetResource.addEvent(newEvent);
		
	}
	
	/**
	 * Returns true if the event can be removed from a resource
	 * @param start Start time of the event
	 * @param resource The room for the event
	 * @return true if the event can be removed from a resource
	 */
	public boolean deleteEvent(long start, String resource){
		Iterator<Resource> resItr = resourceList.iterator();
		while (resItr.hasNext())	{
			Resource resObj = resItr.next();
			if (resObj.getName().equals(resource))	{			
				Iterator<Event> itr = resObj.iterator();
				while (itr.hasNext())	{
					Event e = itr.next();					
					if (e.getStart() == start)
						return resObj.deleteEvent(start);
				}
			}
		}
		return false;	
		
	}
	
	/**
	 * Returns the resource object if the resource can be found by its name
	 * @param r Name of the resource
	 * @return the resource object if the resource can be found by its name
	 */
	public Resource findResource(String r){
		for (int i = 0; i < resourceList.size(); i++)	{
			if (resourceList.get(i).getName().equals(r))
				return resourceList.get(i);
		}
		
		return null;
	}
	
	/**
	 * Returns the list of resources 
	 * @return the list of resources 
	 */
	public List<Resource> getResources(){
		return resourceList;
	}
	
	/**
	 * Returns the list of events in a certain resource
	 * @param resource Name of the resource
	 * @return the list of events in a certain resource
	 */
	public List<Event> getEventsInResource(String resource){
		Iterator<Resource> resItr = resourceList.iterator();
		Resource targetResource = null;
		
		while (resItr.hasNext()) {
			Resource resObj = resItr.next();
			if (resObj.getName().equals(resource)) targetResource = resObj;
		}
		// if resource not found
		if (targetResource == null) return null;

		List<Event> eventList = new ArrayList<> ();
		Iterator<Event> itr = targetResource.iterator();
		while (itr.hasNext())	{
			eventList.add(itr.next());
		}
		
		return eventList;
	}
	
	/**
	 * Returns the list of events between a range
	 * @param start Start time of the range
	 * @param end End time of the range
	 * @return the list of events in a certain resource between a range
	 */
	public List<Event> getEventsInRange(long start, long end){
		List<Event> rangeEvents = new ArrayList<Event> ();
		
		Iterator<Resource> resItr = resourceList.iterator();
				
		while (resItr.hasNext())	{
			Resource resObj = resItr.next();	
			Iterator<Event> itr = resObj.iterator();
			while (itr.hasNext())	{
				Event eventObj = itr.next();
				if ((eventObj.getStart() < start) 
						&& (eventObj.getEnd() > end))
				rangeEvents.add(eventObj);		
			}	
		}
		return rangeEvents;
	}
		
	/**
	 * Returns the list of events in a certain resource between a range
	 * 
	 * @param start Start time of the range
	 * @param end End time of the range
	 * @param resource Name of the resource
	 * @return the list of events in a certain resource between a range
	 */
	public List<Event> getEventsInRangeInResource(long start, long end, String resource){
		List<Event> rangeResourceList = new ArrayList<Event> ();
		Iterator<Resource> resItr = resourceList.iterator();
		Resource targetResource = null;
		
		while (resItr.hasNext())	{
			Resource resObj = resItr.next();
			if (resObj.getName().equals(resource))		targetResource = resObj;	
		}
		
		Iterator<Event> itr = targetResource.iterator();
		
		while (itr.hasNext())	{
			Event eventObj = itr.next();
			if ((eventObj.getStart() < start) 
					&& (eventObj.getEnd() > end))
			rangeResourceList.add(eventObj);	
		}
		
		return rangeResourceList;
	}
	
	/**
	 * Returns the list of all events
	 * @return the list of all events
	 */
	public List<Event> getAllEvents(){
		List<Event> allEvents = new ArrayList<Event> ();
		Iterator<Resource> resItr = resourceList.iterator();
		
		while (resItr.hasNext())	{
			Resource resObj = resItr.next();	
			Iterator<Event> itr = resObj.iterator();
			while (itr.hasNext())	{				
				allEvents.add(itr.next());	
			}
		}
		return allEvents;
	}
	
	/**
	 * Returns the list of events given by a certain organization
	 * @param org Name of the organization
	 * @return the list of events given by a certain organization
	 */
	public List<Event> getEventsForOrg(String org){
		List<Event> orgEvents = new ArrayList<Event> ();
		Iterator<Resource> resItr = resourceList.iterator();
		
		while (resItr.hasNext())	{
			Resource resObj = resItr.next();	
			Iterator<Event> itr = resObj.iterator();			
			while (itr.hasNext())	{
				Event eventObj = itr.next();
				if (eventObj.getOrganization().equals(org))
					orgEvents.add(eventObj);		
			}	
		}
		
		return orgEvents;
	}
		
	
}
