///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  Scheduler.java
// File:             Event.java
// Semester:         CS367 Fall 2015
//
// Author:           Weilan Yang, wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ////////////////////
//
// Pair Partner:     Huilin Hu
// Email:            hhu28@wisc.edu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////////////// 80 columns wide //////////////////////////////////
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Event represents events to be held in .
 */
public class Event implements Interval{
	// fields
	private long start;
	private long end;
	private String name;
	private String resource;
	private String description;
	private String organization;
	public static final String DATE_FORMAT = "MM/dd/yyyy,HH:mm"; 
	
	// constructor
	Event(long start, long end, String name, String resource, 
			String organization, String description){
		this.start = start;
		this.end = end;
		this.name = name;
		this.resource = resource;
		this.organization = organization;
		this.description = description;
	}

	// methods
	/**
	 * Returns the start of an interval.
	 * @return A long indicating staring time point
	 */
	@Override
	public long getStart(){
		return start;
	}

	/**
	 * Returns the end of an interval.
	 * @return A long indicating ending time point
	 */
	@Override
	public long getEnd(){
		return end;
	}

	/**
	 * Returns the name of the event.
	 * @return A String of event name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the resource for the event.
	 * @return A String of resource
	 */
	public String getResource() {
		return resource;
	}
	
	/**
	 * Returns the organization to which this event belongs.
	 * @return A String of organization
	 */
	public String getOrganization(){
		return organization;
	}

	/**
	 * Returns the description of the event.
	 * @return A String of description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Compares two events of type Interval. Returns -1 if the start timestamp 
	 * of this interval-type event is less than the start timestamp of the 
	 * other interval-type event, otherwise returns 1.
	 * @return -1 if this is less, 1 if this is bigger, 0 if equal
	 */
	@Override
	public int compareTo(Interval i) {
		if (start < i.getStart()) return -1;
		if (start > i.getStart()) return 1;
		return 0;
	}
	
	/**
	 * Returns true if the start timestamp of this event is equal to the start 
	 * timestamp of the other event, else returns false.
	 * @param e The other event
	 * @return true if they have same start, false otherwise
	 */
	public boolean equals(Event e) {
		return start == e.getStart();
	}

	/**
	 * Returns true if the otherInterval object overlaps with this interval, 
	 * otherwise returns false. Two intervals overalap, if their timing 
	 * intersection is non-zero. For example, event A(15:00 - 17:00) overlaps 
	 * with event B(16:30 - 17:30), but does not overlap with event 
	 * C(17:00 - 18:00).
	 * @param i
	 * @return true if overlap, false otherwise
	 */
	@Override
	public boolean overlap(Interval i) {
		return !(start > i.getEnd() || end < i.getStart());
	}
	
	/**
	 * Returns the string representation of this event.
	 * @return A string of event representation
	 */
	@Override
	public String toString(){
		DateFormat dispFormat = new SimpleDateFormat(DATE_FORMAT);
		Date startDate = new Date(start);
		Date endDate = new Date(end);
		String dispStart = dispFormat.format(startDate);
		String dispEnd = dispFormat.format(endDate);
		
		String result = "\n";
		result += name + "\n";
		result += "By: " + organization + "\n";
		result += "In: " + resource + "\n";
		result += "Start: " + dispStart + "\n";
		result += "End: " + dispEnd + "\n";
		result += "Description: " + description;
		
		return result;
	}
}
