///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  Scheduler.java
// File:             IntervalBSTIterator.java
// Semester:         CS367 Fall 2015
//
// Author:           Weilan Yang, wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ////////////////////
//
// Pair Partner:     Huilin Hu
// Email:            hhu28@wisc.edu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////////////// 80 columns wide //////////////////////////////////
import java.util.*;


/**
 * The IntervalBSTIterator traverses the IntervalBST nodes using in-order 
 * traversal and returns the nodes sorted by their start timestamp in 
 * ascending order. 
 *
 * @author Weilan Yang
 */
public class IntervalBSTIterator<K extends Interval> implements Iterator<K> {
	// fields
	private Stack<IntervalBSTnode<K>> stack; //for keeping track of nodes
	
	// constructor
	public IntervalBSTIterator(IntervalBSTnode<K> root) {
		stack = new Stack<IntervalBSTnode<K>>();
		pushNodes(root);
	} 
	
	// methods
	/**
	 * Helper method for recursively pushing the nodes into the stack, in 
	 * ascending order of start timestamp.
	 * @param root The root of a BST tree.
	 */
	private void pushNodes(IntervalBSTnode<K> root) {
		if (root == null) return;
		pushNodes(root.getRight());
		stack.push(root);
		pushNodes(root.getLeft());
	}
	
    public boolean hasNext() {
    	return !stack.isEmpty();
    }

    public K next() {
    	return stack.pop().getData();
    }

    public void remove() {
        // DO NOT CHANGE: you do not need to implement this method
        throw new UnsupportedOperationException();
    }
}