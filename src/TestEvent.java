
public class TestEvent {
	public static void main(String[] args) {
		long start1 = System.currentTimeMillis();
		long start2 = System.currentTimeMillis() + 1;
		long end1 = System.currentTimeMillis() + 2;
		long end2 = System.currentTimeMillis() + 3;
		
		Event e1 = new Event(start1, end1, "Google Speech", "Main Hall", 
				"Madison Tech", "CEO of GoogleMadison will speak on the latest and greatest from GoogleMadison.  Projector needed.");
		Event e2 = new Event(start2, end2, "Happy Hour" ,"Adventure Centre", 
				"Madtown Singers", "Combination of food and entertainment.  They need a full set-up.");
		
		System.out.println("e1 comparedTo e2: " + e1.compareTo(e2));
		System.out.println("e1 comparedTo e1: " + e1.compareTo(e1));
		System.out.println("e1 equals e2: " + e1.equals(e2));
		System.out.println("e1 overlaps e2: " + e1.overlap(e2));
		System.out.println(e1);
	}
}
