import java.util.Iterator;


public class TestBST {
	public static void main(String[] args) {
		long start1 = System.currentTimeMillis();
		long end1 = System.currentTimeMillis() + 1;
		long start2 = System.currentTimeMillis() + 2;
		long end2 = System.currentTimeMillis() + 3;
		long start3 = System.currentTimeMillis() + 4;
		long end3 = System.currentTimeMillis() + 5;
		
		Event e1 = new Event(start1, end1, "e1", "r1", "o1", "d1");
		Event e2 = new Event(start2, end2, "e2", "r2", "o2", "d2");
		Event e3 = new Event(start3, end3, "e3", "r3", "o3", "d3");
		Event e4 = new Event(start1-1, end3+1, "e4", "r4", "o4", "overlaps");
		
		
		//IntervalBSTnode<Event> n1 = new IntervalBSTnode<Event>(e1);
		//IntervalBSTnode<Event> n2 = new IntervalBSTnode<Event>(e2);
		//IntervalBSTnode<Event> n3 = new IntervalBSTnode<Event>(e3);
		//IntervalBSTnode<Event> n4 = new IntervalBSTnode<Event>(e4);
		
		IntervalBST<Event> tree = new IntervalBST<Event>();
		System.out.println("Tree is empty: " + tree.isEmpty());
		tree.insert(e2);
		tree.insert(e1);
		tree.insert(e3);
		
		System.out.println("Tree is empty: " + tree.isEmpty());
		System.out.println("tree size: " + tree.size());
		System.out.println("lookup e1: " + tree.lookup(e1));
		System.out.println("lookup e4: " + tree.lookup(e4));
		
		System.out.println("iteration: ......");
		Iterator<Event> itr = tree.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next().getName());
		}
		
		System.out.println("delete e4: " + tree.delete(e4));
		System.out.println("delete e1: " + tree.delete(e1));
		
		System.out.println("tree size: " + tree.size());
		System.out.println("iteration: ......");
		itr = tree.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next().getName());
		}
		
		System.out.println("delete e1: " + tree.delete(e2));
		
		System.out.println("tree size: " + tree.size());
		System.out.println("iteration: ......");
		itr = tree.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next().getName());
		}
		
	}
}
